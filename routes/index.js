var express = require('express');
var router = express.Router();
var fs = require('fs');
var request = require('request');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile('weather.html', { root:  'public' });
});

router.get('/getcity',function(req,res,next) {
    console.log("In getcity route");
	var query = ""+req.query.q;
	var myRe = new RegExp("^" + query.toLowerCase());
	console.log(myRe);
	var cities;
	var jsonresult = [];
	fs.readFile(__dirname + '/cities.dat.txt',function(err,data) {
		if(err) throw err;
        cities = data.toString().split("\n");
        for(var i = 0; i < cities.length; i++) {
			if(query == "undefined"){
				if(cities[i] != "") jsonresult.push({city:cities[i]});
			}
			else{
				var result = cities[i].toLowerCase().search(myRe);
				if(result != -1){
					if(cities[i] != "") jsonresult.push({city:cities[i]});
				}
			}
        }
		res.status(200).json(jsonresult);
	})

});

router.get('/pokemon', function(req, res) {
    console.log("In Pokemon");
    res.send(pokemon);
});

var map = "https://maps.googleapis.com/maps/api/js?key=AIzaSyD_WxzehZmo21NVhT8kt0L5KPw7wqY0Oc4&callback=initMap";
router.get('/map', function(req,res) {
    console.log("map");
    request(map).pipe(res);
});


module.exports = router;
